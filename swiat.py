# -*- coding: utf-8 -*-

"""
PyQt4 projekt
Wojciech Molicki

PyVirtualWorld v1.0 @ 2014
"""

import random

from fabrykaobiektow import FabrykaOrganizmow

class ClassProtectionViolation(Exception):
	pass

class Swiat(object):

	ORGANIZMOW_NA_STARCIE = 20
	NAJWYZSZA_INICJATYWA = 5
	organizmyNazwy = ["ciern", "guarana", "trawa", "guziec", "zolw", "leniwiec", "wilk", "owca"]

	def __init__(self):

		# (x, y) : organizm
		self._organizmy = {}
		self._organizmySwiezoNarodzone = {}
		self._komunikaty = []

		self._dodajPoczatkowe()

	def _dodajPoczatkowe(self):

		dodano = 0
		while dodano < Swiat.ORGANIZMOW_NA_STARCIE:
			x = random.randint(0,19)
			y = random.randint(0,19)
			if (x, y) not in self._organizmy:
				dodano += 1
				self.dodajOrganizm(Swiat.organizmyNazwy[random.randint(0,7)], x, y)

	def dodajOrganizm(self, typ, x, y):
		self._organizmy[(x, y)] = FabrykaOrganizmow.zwrocOrganizm(self, typ, x, y)

	def dodajOrganizmPodczasTury(self, typ, x, y):
		self._organizmySwiezoNarodzone[(x, y)] = FabrykaOrganizmow.zwrocOrganizm(self, typ, x, y)

	def wykonajTure(self):

		#print("swiatWykonuje")
		self._komunikaty = []
		ileOrganizmow = 0
		for i in range(Swiat.NAJWYZSZA_INICJATYWA, -1, -1):
			for key in self._organizmy:
				o = self._organizmy[key]
				if o.alive and o.inicjatywa == i:
					ileOrganizmow += 1
					o.akcja()
		self.dodajKomunikat("ilosc organizmow = %d" % ileOrganizmow)				
		self._organizmy = { k : v for k, v in self._organizmy.items() if v.alive == True }
		self.przywrocWlasnosciOrganizmow()


	def kolizjaZObiektem(self, x, y):

		if (x, y) in self._organizmy:
			return self._organizmy[(x, y)]
		if (x, y) in self._organizmySwiezoNarodzone:
			return self._organizmySwiezoNarodzone[(x, y)]
		return None

	def wyczyscOrganizmy(self):
		self._organizmy = {}

	def wczytajZPliku(self, typ, x, y, age, sila):
		self._organizmy[(x, y)] = FabrykaOrganizmow.zwrocOrganizmIUstawAtrybuty(self, typ, x, y, age, sila)


	def dodajKomunikat(self, komunikat):

		self._komunikaty.append(komunikat)

	def przywrocWlasnosciOrganizmow(self):

		self._organizmy.update(self._organizmySwiezoNarodzone)
		self._organizmySwiezoNarodzone = {}
		for key in self._organizmy:
			self._organizmy[key].postarz()
			self._organizmy[key].rozmnazalSie = False

	@property
	def organizmy(self):
	    return self._organizmy
	@organizmy.setter
	def organizmy(self, value):
	    raise ClassProtectionViolation("This shouldnt be modified from outside classes!")

	@property
	def komunikaty(self):
	    return self._komunikaty
	@komunikaty.setter
	def komunikaty(self, value):
	    self._komunikaty = value
	
	

		

