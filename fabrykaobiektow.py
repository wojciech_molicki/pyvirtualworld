# -*- coding: utf-8 -*-

"""
PyQt4 projekt
Wojciech Molicki

PyVirtualWorld v1.0 @ 2014
"""

from roslina import Guarana, Trawa, Ciern
from zwierze import Wilk, Owca, Guziec, Leniwiec, Zolw

class WrongOrganismException(Exception):
	pass

class FabrykaOrganizmow(object):

	def __init__(self):
		raise NotImplementedError

	@staticmethod
	def zwrocOrganizm(swiat, typ, x, y):
		if typ == "ciern":
			return Ciern(swiat, x, y)
		elif typ == "guarana":
			return Guarana(swiat, x, y)
		elif typ == "trawa":
			return Trawa(swiat, x, y)
		elif typ == "leniwiec":
			return Leniwiec(swiat, x, y)
		elif typ == "zolw":
			return Zolw(swiat, x, y)
		elif typ == "guziec":
			return Guziec(swiat, x, y)
		elif typ == "wilk":
			return Wilk(swiat, x, y)
		elif typ == "owca":
			return Owca(swiat, x, y)
		else:
			raise WrongOrganismException("ObjectFactory returned None!")

	@staticmethod
	def zwrocOrganizmIUstawAtrybuty(swiat, typ, x, y, age, sila):
		o = None
		if typ == "ciern":
			o = Ciern(swiat, x, y)
		elif typ == "guarana":
			o = Guarana(swiat, x, y)
		elif typ == "trawa":
			o = Trawa(swiat, x, y)
		elif typ == "leniwiec":
			o = Leniwiec(swiat, x, y)
		elif typ == "zolw":
			o = Zolw(swiat, x, y)
		elif typ == "guziec":
			o = Guziec(swiat, x, y)
		elif typ == "wilk":
			o = Wilk(swiat, x, y)
		elif typ == "owca":
			o = Owca(swiat, x, y)
		else:
			raise WrongOrganismException("ObjectFactory returned None!")
		o.sila = sila
		o.age = age
		return o
