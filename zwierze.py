# -*- coding: utf-8 -*-

"""
PyQt4 projekt
Wojciech Molicki

PyVirtualWorld v1.0 @ 2014
"""

import random

from organizm import Organizm

class Zwierze(Organizm):

	def __init__(self, swiat, x, y):

		super().__init__(swiat, x, y)

	def akcja(self):

		if not self.alive:
			return

		pole = self.wybierzLosowePole()
		kolidowany = self.swiat.kolizjaZObiektem(pole[0], pole[1])

		if kolidowany is not None:
			self.kolizja(kolidowany)
			#rozmnazanie
			if self.name == kolidowany.name:
				pole = self.wybierzLosoweWolnePole()
				if pole is not None:
					self.swiat.dodajKomunikat("(%d,%d): Rozmnazanie zwierzat: %s!" % \
						(self.x, self.y, self.name))
					self.swiat.dodajOrganizmPodczasTury(self.name, pole[0], pole[1])
			#walka
			else:
				if self.sila >= kolidowany.sila:
					kolidowany.alive = False
					self.swiat.dodajKomunikat("(%d,%d): atak %s, broni sie %s. Wygrywa %s!" % \
					 (self.x, self.y, self.name, kolidowany.name, self.name))
					self.ustawPolozenie(kolidowany.getPole())
					kolidowany.kolizja(self)
				else:
					self.alive = False
					self.swiat.dodajKomunikat("(%d,%d): atak %s, broni sie %s. Wygrywa %s!" % \
						(self.x, self.y, self.name, kolidowany.name, kolidowany.name))
		else:
			self.ustawPolozenie(pole)

	def kolizja(self, kolidowany):
		pass
		

class Owca(Zwierze):
	def __init__(self, swiat, x, y):
		super().__init__(swiat, x, y)
		#print("tworze owce")
		self.name = "owca"
		self.inicjatywa = 4
		self.sila = 4

class Wilk(Zwierze):
	def __init__(self, swiat, x, y):
		super().__init__(swiat, x, y)
		#print("tworze wilka")
		self.name = "wilk"
		self.inicjatywa = 5
		self.sila = 9

class Leniwiec(Zwierze):
	def __init__(self, swiat, x, y):
		super().__init__(swiat, x, y)
		#print("tworze wilka")
		self.name = "leniwiec"
		self.inicjatywa = 1
		self.sila = 2
		self._ruszalSie = False

	def akcja(self):
		if self._ruszalSie == True:
			self._ruszalSie = False
			self.swiat.dodajKomunikat("(%d, %d) Leniwiec postanowil sie nie ruszac!" % (self.x, self.y))
		else:
			self._ruszalSie = True
			super().akcja()

class Zolw(Zwierze):
	def __init__(self, swiat, x, y):
		super().__init__(swiat, x, y)
		#print("tworze wilka")
		self.name = "zolw"
		self.inicjatywa = 1
		self.sila = 2

	def akcja(self):
		if random.randint(0, 50) < 100:
			self.swiat.dodajKomunikat("(%d, %d) Zolw postanowil sie nie ruszac!" % (self.x, self.y))
		else:
			super().akcja()

class Guziec(Zwierze):
	def __init__(self, swiat, x, y):
		super().__init__(swiat, x, y)
		#print("tworze wilka")
		self.name = "guziec"
		self.inicjatywa = 3
		self.sila = 7
		self._bonusSila = 0

	@property
	def sila(self):
	    return self._sila + self._bonusSila
	@sila.setter
	def sila(self, value):
	    self._sila = value

	def akcja(self):
		if self.alive == False:
			return
		super().akcja()
		if self.alive:
			self._bonusSila += 1
			self.swiat.dodajKomunikat("(%d %d) Sila Guzca zwieksza sie!" % (self.x, self.y))


	
