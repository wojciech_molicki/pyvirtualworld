# -*- coding: utf-8 -*-

"""
PyQt4 projekt
Wojciech Molicki

PyVirtualWorld v1.0 @ 2014
"""

import random

class Organizm(object):

	def __init__(self, swiat, x, y):
		self._name = ""
		self._swiat = swiat
		self._color = "black"
		self._age = -1
		self._x = x
		self._y = y
		self._sila = 0
		self._alive = True
		self._inicjatywa = 0
		self._rozmnazalSie = True

	@property
	def swiat(self):
	    return self._swiat
	@swiat.setter
	def swiat(self, value):
	    self._swiat = value
	
	@property
	def x(self):
	    return self._x
	@x.setter
	def x(self, value):
	    self._x = value

	@property
	def name(self):
	    return self._name
	@name.setter
	def name(self, value):
	    self._name = value

	@property
	def rozmnazalSie(self):
	    return self._rozmnazalSie
	@rozmnazalSie.setter
	def rozmnazalSie(self, value):
	    self._rozmnazalSie = value
	
	@property
	def y(self):
	    return self._y
	@y.setter
	def y(self, value):
	    self._y = value

	@property
	def color(self):
	    return self._color
	@color.setter
	def color(self, value):
	    self._foo = value

	@property
	def age(self):
	    return self._age
	@age.setter
	def age(self, value):
	    self._age = value

	@property
	def alive(self):
	    return self._alive
	@alive.setter
	def alive(self, value):
	    self._alive = value

	@property
	def sila(self):
	    return self._sila
	@sila.setter
	def sila(self, value):
	    self._sila = value
	
	@property
	def inicjatywa(self):
	    return self._inicjatywa
	@inicjatywa.setter
	def inicjatywa(self, value):
	    self._inicjatywa = value
	

	def zwrocPrzylegajacePola(self):
		pola = [(self.x + 1, self.y), (self.x - 1, self.y), (self.x, self.y + 1), (self.x, self.y - 1), \
				(self.x + 1, self.y + 1), (self.x + 1, self.y - 1), (self.x - 1, self.y - 1), (self.x - 1, self.y + 1)]
		return pola

	def miesciSieWPlanszy(self, x, y):
		return x < 20 and y < 20 and x >= 0 and y >= 0

	def wybierzLosowePole(self):
		sasiednie = self.zwrocPrzylegajacePola()
		random.shuffle(sasiednie)
		for pole in sasiednie:
			if (self.miesciSieWPlanszy(pole[0], pole[1])):
				return pole

	def wybierzLosoweWolnePole(self):
		sasiednie = self.zwrocPrzylegajacePola()
		random.shuffle(sasiednie)
		for pole in sasiednie:
			if (self.miesciSieWPlanszy(pole[0], pole[1]) and self.swiat.kolizjaZObiektem(pole[0], pole[1]) is None):
				return pole
		return None


	def ustawPolozenie(self, P):
		self.x = P[0]
		self.y = P[1]


	def getPole(self):
		return (self.x, self.y)

	def postarz(self):
		self.age += 1