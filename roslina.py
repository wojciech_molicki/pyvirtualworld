# -*- coding: utf-8 -*-

"""
PyQt4 projekt
Wojciech Molicki

PyVirtualWorld v1.0 @ 2014
"""

import random

from organizm import Organizm

class Roslina(Organizm):
	def __init__(self, swiat, x, y):

		super().__init__(swiat, x, y)
		self.pRozmnazania = 0.5
		self.inicjatywa = 0
		self.sila = 0

	def akcja(self):

		if not self.alive:
			return

		if self.rozmnazalSie == False and self.pRozmnazania * 100 > random.randint(0,100):
			pole = self.wybierzLosoweWolnePole()
			if pole is not None:
				#self.swiat.dodajKomunikat("(%d,%d): Rozmnazanie rosliny: %s!" % (self.x, self.y, self.name))
				self.swiat.dodajOrganizmPodczasTury(self.name, pole[0], pole[1])


	def kolizja(self, kolidowany):

		pass

class Trawa(Roslina):
	def __init__(self, swiat, x, y):
		super().__init__(swiat, x, y)
		#print("tworze wilka")
		self.name = "trawa"


class Guarana(Roslina):
	def __init__(self, swiat, x, y):
		super().__init__(swiat, x, y)
		#print("tworze wilka")
		self.name = "guarana"
		self.sila = 2

	def kolizja(self, kolidowany):
		kolidowany.sila += 2
		self.swiat.dodajKomunikat("Guarana zwieksza sile %s!" % kolidowany.name)


class Ciern(Roslina):
	def __init__(self, swiat, x, y):
		super().__init__(swiat, x, y)
		#print("tworze wilka")
		self.name = "ciern"