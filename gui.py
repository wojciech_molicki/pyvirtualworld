# -*- coding: utf-8 -*-

"""
PyQt4 projekt
Wojciech Molicki

PyVirtualWorld v1.0 @ 2014
"""

import sys
import random

from PyQt4 import QtGui
from PyQt4 import QtCore

from swiat import Swiat

class Gui(QtGui.QWidget):

    animalsToColors = {"owca" : (255, 255, 255), "guarana" : (255, 216, 0), "ciern" : (127, 51, 0), \
     "guziec" : (137, 12, 255), "leniwiec" : (255, 12, 36), "trawa" : (0, 127, 14), \
      'wilk' : (128, 128, 128), "zolw" : (0, 255, 76)}
    
    
    def __init__(self, organizmy, parent=None,):

        super().__init__(parent)
        self._organizmy = organizmy

    def paintEvent(self, e):

        qp = QtGui.QPainter(self)
        self.drawRectangles(qp)

    def drawRectangles(self, qp):
        
        qp.setPen(QtCore.Qt.NoPen)

        for key in self._organizmy:

            o = self._organizmy[key]
            if o is None:
                continue
            qp.setBrush(QtGui.QColor(*Gui.animalsToColors[o.name])) #unpacking tuple
            qp.drawRect(o.x*20, o.y*20, 20, 20)

    def keyPressEvent(self, event):
        pass

    @property
    def organizmy(self):
        return self._organizmy
    @organizmy.setter
    def organizmy(self, value):
        self._organizmy = value
    

class MainWindow(QtGui.QMainWindow):
    
    def __init__(self, swiat):
        super().__init__()
        self.swiat = swiat
        self.initUI()
        
    def initUI(self):  
        
        #menu bar and menu items
        exitAction = QtGui.QAction(QtGui.QIcon('exit.ico'), '&Exit', self)        
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(QtGui.qApp.quit)

        saveAction = QtGui.QAction(QtGui.QIcon('save.ico'), '&Save', self)        
        saveAction.setShortcut('Ctrl+S')
        saveAction.triggered.connect(self.showFSaveDialog)

        loadAction = QtGui.QAction(QtGui.QIcon('load.ico'), '&Load', self)        
        loadAction.setShortcut('Ctrl+L')
        loadAction.triggered.connect(self.showFOpenDialog)
        
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&Menu')

        fileMenu.addAction(saveAction)
        fileMenu.addAction(loadAction)
        fileMenu.addAction(exitAction)
        
        #text field
        self.logOutput = QtGui.QPlainTextEdit(self)
        self.logOutput.setReadOnly(True)

        fmt = QtGui.QTextCharFormat()
        fmt.setFontPointSize(12)
        self.logOutput.setCurrentCharFormat(fmt)

        self.logOutput.setStyleSheet("QPlainTextEdit { border: 0px solid black }")
        self.logOutput.appendHtml("<b><font color='green'>Welcome to PyVirtualWorld!</font></b>")
        self.logOutput.setReadOnly(True)

        self.logOutput.setGeometry(415,25,380,365)

        # plansza
        self.col = QtGui.QColor(255, 0, 255)       
        self.square = Gui(self.swiat.organizmy, self)
        
        self.square.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.square.customContextMenuRequested.connect(self.openMenu)
    
        self.square.setGeometry(5, 25, 401, 401)
        
        #next turn button
        newTurnButton = QtGui.QPushButton("Nowa Tura", self)
        newTurnButton.move(414, 391)
        newTurnButton.resize(382, 36)
        newTurnButton.clicked.connect(self.handleNewTurn)
        
        #glowne okno
        self.setGeometry(300, 300, 800, 432)
        self.setWindowTitle("pyVirtualWorld v1.0")
        self.setWindowIcon(QtGui.QIcon('globe.ico'))
        self.setFixedSize(self.size())
        self.show()

    def openMenu(self, pos):

        rmenu = QtGui.QMenu(self.square)
        trawa = rmenu.addAction(QtGui.QIcon('trawa.png'), "Trawa")
        guarana = rmenu.addAction(QtGui.QIcon('guarana.png'), "Guarana")
        ciern = rmenu.addAction(QtGui.QIcon('ciern.png'), "Ciern")
        rmenu.addSeparator()
        wilk = rmenu.addAction(QtGui.QIcon('wilk.png'), "Wilk")
        owca = rmenu.addAction(QtGui.QIcon('owca.png'), "Owca")
        guziec = rmenu.addAction(QtGui.QIcon('guziec.png'), "Guziec")
        leniwiec = rmenu.addAction(QtGui.QIcon('leniwiec.png'), "Leniwiec")
        zolw = rmenu.addAction(QtGui.QIcon('zolw.png'), "Zolw")

        action = rmenu.exec_(self.mapToGlobal(pos))

        if action == trawa:
            self.swiat.dodajOrganizm("trawa", pos.x() // 20, pos.y() // 20)
        elif action == guarana:
            self.swiat.dodajOrganizm("guarana", pos.x() // 20, pos.y() // 20)
        elif action == zolw:
            self.swiat.dodajOrganizm("zolw", pos.x() // 20, pos.y() // 20)
        elif action == ciern:
            self.swiat.dodajOrganizm("ciern", pos.x() // 20, pos.y() // 20)
        elif action == wilk:
            self.swiat.dodajOrganizm("wilk", pos.x() // 20, pos.y() // 20)
        elif action == owca:
            self.swiat.dodajOrganizm("owca", pos.x() // 20, pos.y() // 20)
        elif action == guziec:
            self.swiat.dodajOrganizm("guziec", pos.x() // 20, pos.y() // 20)
        elif action == leniwiec:
            self.swiat.dodajOrganizm("leniwiec", pos.x() // 20, pos.y() // 20)

        self.square.repaint()

    def showFOpenDialog(self):
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file', 
                'save.dat')
        f = open(fname, 'r')
        
        with f:      
            self.swiat.wyczyscOrganizmy()
            for line in f: 
                line = line.split(' ')
                self.swiat.wczytajZPliku(line[0],int(line[1]),int(line[2]), int(line[3]), int(line[4].rstrip()))
            self.square.organizmy = self.swiat.organizmy
            self.square.repaint()

    def showFSaveDialog(self):
        fname = QtGui.QFileDialog.getSaveFileName(self, 'Save file', 
                'save.dat')
        f = open(fname, 'w')

        with f: 
            for key in self.swiat.organizmy:
                o = self.swiat.organizmy[key]      
                f.write("%s %s %s %s %s\n" % (o.name, str(o.x), str(o.y), str(o.age), str(o.sila)))


    def setTextBoxValue(self, text):
        self.logOutput.appendHtml(text)

    def handleNewTurn(self):
        self.swiat.wykonajTure()
        self.logOutput.clear()
        self.logOutput.appendPlainText('\n'.join(self.swiat.komunikaty))
       
        self.square.organizmy = self.swiat.organizmy
        self.square.repaint()


def main():
    app = QtGui.QApplication(sys.argv)
    swiat = Swiat()
    mw = MainWindow(swiat)
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
    print("beka")